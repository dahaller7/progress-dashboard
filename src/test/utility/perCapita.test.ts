import { perCapita } from '@App/utility/perCapita';
import { sameDate } from '@App/utility/perCapita';
import { timeParse } from 'd3';

import { miles } from '@App/test/data/miles';
import { population } from '@App/test/data/population';
import { milesPerCapita } from '@App/test/data/milesPerCapita';

let timeParser = timeParse("%Y-%m-%d");

// --------------
// sameDate tests
// --------------

test('sameDate returns false for different dates', () => {
  let date1 = timeParser('1959-01-01');
  let date2 = timeParser('1960-01-01');
  expect(sameDate(date1, date2)).toBe(false);
});

test('sameDate returns true for the same dates', () => {
  let date1 = timeParser('1960-01-01');
  let date2 = timeParser('1960-01-01');
  expect(sameDate(date1, date2)).toBe(true);
});


// --------------
// perCapita tests
// --------------

let milesParsed = miles.map((datum) => {
  return {
    date: timeParse("%Y-%m-%d")(datum.date.substr(0, 10)),
    value: datum.miles
  }
});

let populationParsed = population.map((datum) => {
  return {
    date: timeParse("%Y-%m-%d")(datum.date.substr(0, 10)),
    population: datum.population
  }
});


test('perCapita for miles works', () => {
  expect(
    perCapita(milesParsed, populationParsed, 1000000 / 1000).map((data) => {
      return {
        /**
        *  The test doesn't include the date because I copied the resulting object
        *  from the debugger, where the date was a date object, and the copy didn't
        *  convert the date to a string, so we take the date out.  The values are 
        *  correct.
        */
        date: {},
        perCapitaValue: data.perCapitaValue
      }
    })
  ).toStrictEqual(milesPerCapita);
})





export { }