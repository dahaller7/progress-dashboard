/**
 * Function to turn total data into data per capita, based on the population at the time
 * @param data Array of data objects by date.
 * @param population Array of population data by date.
 * @param scalingFactor data and population value may be in 'thousands' or 'millions',
 * requiring scaling to reach an accurate final number when taking data / population.
 * scalingFactor should be equal to (data units) / (population units).
 * @returns Array of data per capita by date.
 */
export function perCapita(data: { date: Date, value: number }[], population: { date: Date, population: string }[], scalingFactor: number) {

  let dataPerCapita: {
    date: Date;
    perCapitaValue: number;
  }[] = [];

  data.forEach((data) => {
    let populace = population.filter((popData) => {
      return sameDate(data.date, popData.date);
    });
    dataPerCapita.push({
      date: data.date,
      perCapitaValue: (data.value / +populace[0].population) * scalingFactor
    });
  })

  return dataPerCapita;
}

/**
 * Simple function to compare two dates for equality.
 * @param date1 First Date object.
 * @param date2 Second Date object.
 * @returns true if the dates are the same, false otherwise.
 */
export function sameDate(date1: Date, date2: Date) {
  return date1.getTime() == date2.getTime();
}
