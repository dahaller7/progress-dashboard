import { writable } from 'svelte/store';

const availableRegions: Array<string | string[]> = ['United States'];

export const regionStore = writable(availableRegions);
export const currentRegionStore = writable("United States");
