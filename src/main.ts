import App from './App.svelte';

const app = new App({
	target: document.body,
	props: {
		name: 'Progress Dashboard'
	}
});

export default app;